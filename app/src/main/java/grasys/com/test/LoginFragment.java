package grasys.com.test;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by VuHung on 4/18/2015.
 */
public class LoginFragment extends Fragment {
    private Button login_button;
    private TextView login_forgot_password;
    private TextView login_register;
    private LinearLayout login_container;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login, container, false);

//        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        //Setup anim with desired properties
//        anim.setInterpolator(new LinearInterpolator());
//        anim.setRepeatCount(Animation.INFINITE); //Repeat animation indefinitely
//        anim.setDuration(1500); //Put desired duration per anim cycle here, in milliseconds

        login_button = (Button) view.findViewById(R.id.login_button);

        // container
        login_container = (LinearLayout) view.findViewById(R.id.login_container);

        stopAnimation(login_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check email valid, trim
                startAnimation(login_button);
            }
        });

        // set text link clickable
        login_forgot_password = (TextView) view.findViewById(R.id.login_forgot_password);
        login_forgot_password.setText(Html.fromHtml("<a href='https://secure.nicovideo.jp/secure/remind_pass'>Forgot your password?</a>"));
        login_forgot_password.setMovementMethod(LinkMovementMethod.getInstance());

        // set text link clickable
        login_register = (TextView) view.findViewById(R.id.login_register);
        login_register.setText(Html.fromHtml("<a href='https://account.nicovideo.jp/register/email?site=niconico'>Create a new account</a>"));
        login_register.setMovementMethod(LinkMovementMethod.getInstance());

        return view;
    }

    public void startAnimation(Button button) {
        // set drawable
        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.rotating_loading, 0, 0, 0);
        Drawable[] drawables = button.getCompoundDrawables();
        for (Drawable drawable : drawables) {
            if (drawable != null && drawable instanceof Animatable) {
                ((Animatable) drawable).start();
            }
        }

        setViewAndChildrenEnabled(login_container, false);
    }

    private void stopAnimation(Button button) {
        // set drawable
        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.login_button_go, 0, 0, 0);
        button.setAnimation(null);
        setViewAndChildrenEnabled(login_container, true);
    }

    /**
     * khi đang login không cho phép sửa gì hết, nếu có lỗi đăng nhập thì trở về trạng thái bt
     *
     * @param view
     * @param enabled
     */
    private static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    /**
     * check email format valid ?
     *
     * @param email
     * @return
     */
    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

}
